//
//  NYCSchoolsAPI.swift
//  20210504-SatoruIshii-NYCSchools
//
//  Created by Satoru Ishii on 5/4/21.
//

import Foundation
import Combine

enum APIError: Error {
    case network(description: String)
    case decoding(description: String)
}
protocol NYCSchoolsConnectorable {
    func nycSchools() -> AnyPublisher<[NYCSchoolsResponse], APIError>
}

// The Connector for The NYC Schools Service
class NYCSchoolsConnector : NYCSchoolsConnectorable {
    // Get Now_PlayingResponse data
    func nycSchools() -> AnyPublisher<[NYCSchoolsResponse], APIError> {
        let urlComponents = self.makeNYCSchoolsRequestUrl()
        return publishConnector(urlComponents: urlComponents)
    }
    
    // Create the NYC Schools request url
    private func makeNYCSchoolsRequestUrl() -> URLComponents {
        var urlComp = URLComponents()
        urlComp.scheme = ConstantsTable.Scheme
        urlComp.host = ConstantsTable.Host
        urlComp.path = ConstantsTable.Path + ConstantsTable.NYCSchoolsReq
        
        return urlComp
    }
    
    // Connect Service API, Downloading and Publish the data
    private func publishConnector(urlComponents components: URLComponents) -> AnyPublisher<[NYCSchoolsResponse], APIError> {
        guard let url = components.url else {
            let error = APIError.network(description: "Can't create URL")
            return Fail(error: error).eraseToAnyPublisher()
        }
        return URLSession.shared.dataTaskPublisher(for: URLRequest(url: url))
          .mapError { error in
            .network(description: error.localizedDescription)
          }
          .flatMap(maxPublishers: .max(1)) { pair in
                self.decode(pair.data)
          }
          .eraseToAnyPublisher()
    }
    
    // Decode json data to NowPlayingResponse struct data
    private func decode(_ data: Data) -> AnyPublisher<[NYCSchoolsResponse], APIError> {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .secondsSince1970
        return Just(data)
          .decode(type: [NYCSchoolsResponse].self, decoder: decoder)
          .mapError { error in
            .decoding(description: error.localizedDescription)
          }
          .eraseToAnyPublisher()
    }
}
