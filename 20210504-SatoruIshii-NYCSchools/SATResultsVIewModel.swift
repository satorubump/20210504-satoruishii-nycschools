//
//  SATResultsVIewModel.swift
//  20210504-SatoruIshii-NYCSchools
//
//  Created by Satoru Ishii on 5/5/21.
//

import Foundation
import SwiftUI
import Combine
import MapKit

// SAT Results View Model
class SATResultsViewModel : ObservableObject {
    // SAT Results Data
    @Published var satResultsResponse : [SATResultsResponse]?
    
    let satResultsConnector = SATResultsConnector()
    private var disposables = Set<AnyCancellable>()

    // Subscribe SAT Results Data
    func getSATResults(dbn: String) -> Void {
        // Fetch the data
        satResultsConnector.satResults(dbn: dbn)
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { [weak self] value in
                    guard let self = self else { print("sink guard nil"); return }
                    switch value {
                    case .failure:
                        self.satResultsResponse = nil
                        print("sink failure")
                    case .finished:
                        //print("sink finished")
                        break
                    }
                },
                receiveValue: { [weak self] satResultsResponses in
                    self!.satResultsResponse = satResultsResponses
                    //print("receiveValue")
                })
            .store(in: &disposables)
        
    }
    
    func getCoordinate(location: String) -> CLLocationCoordinate2D? {
        var s = ""
        var isIn = false
        for c in location {
            if c == "(" {
                isIn = true
                continue
            }
            else if c == ")" {
                isIn = false
                continue
            }
            if isIn {
                s += String(c)
            }
        }
        let arc = s.components(separatedBy: ", ")
        let coordinate = CLLocationCoordinate2DMake(Double(arc[0])!, Double(arc[1])!)
        return coordinate
    }
    //
    func getAddress(location: String) -> String {
        var n = 0
        for c in location {
            if c == "(" {
                break
            }
            n += 1
        }
        let address = String(location.prefix(n))
        return address
    }

}

