//
//  SATResultsAPI.swift
//  20210504-SatoruIshii-NYCSchools
//
//  Created by Satoru Ishii on 5/5/21.
//

import Foundation
import Combine

protocol SATResultsConnectoable {
    func satResults(dbn: String) -> AnyPublisher<[SATResultsResponse], APIError>
}

// The Connector for The NYC Schools Service
class SATResultsConnector : SATResultsConnectoable {
    // Get Now_PlayingResponse data
    func satResults(dbn: String) -> AnyPublisher<[SATResultsResponse], APIError> {
        let urlComponents = self.makeSATResultsRequestUrl(dbn: dbn)
        return satPublishConnector(urlComponents: urlComponents)
    }
    
    // Create the NYC Schools request url
    private func makeSATResultsRequestUrl(dbn: String) -> URLComponents {
        var urlComp = URLComponents()
        urlComp.scheme = ConstantsTable.Scheme
        urlComp.host = ConstantsTable.Host
        urlComp.path = ConstantsTable.Path + ConstantsTable.SATResultsReq
        urlComp.queryItems = [
            URLQueryItem(name: ConstantsTable.DBN, value: dbn)
        ]
        return urlComp
    }
    
    // Connect Service API, Downloading and Publish the data
    private func satPublishConnector(urlComponents components: URLComponents) -> AnyPublisher<[SATResultsResponse], APIError> {
        guard let url = components.url else {
            let error = APIError.network(description: "Can't create URL")
            return Fail(error: error).eraseToAnyPublisher()
        }
        return URLSession.shared.dataTaskPublisher(for: URLRequest(url: url))
          .mapError { error in
            .network(description: error.localizedDescription)
          }
            .print()
          .flatMap(maxPublishers: .max(1)) { pair in
                self.decode(pair.data)
          }
          .eraseToAnyPublisher()
    }
    
    // Decode json data to NowPlayingResponse struct data
    private func decode(_ data: Data) -> AnyPublisher<[SATResultsResponse], APIError> {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .secondsSince1970

        return Just(data)
          .decode(type: [SATResultsResponse].self, decoder: decoder)
          .print()
          .mapError { error in
            .decoding(description: error.localizedDescription)
          }
          .eraseToAnyPublisher()
    }
}
