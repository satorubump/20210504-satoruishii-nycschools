//
//  SchoolsListViewModel.swift
//  20210504-SatoruIshii-NYCSchools
//
//  Created by Satoru Ishii on 5/4/21.
//

import Foundation
import SwiftUI
import Combine

// The View Model for 2017 DOE High School Directory
class NYCSchoolsViewModel : ObservableObject {
    @Published var nycSchoolsResponse : [NYCSchoolsResponse]?

    let nycSchoolsConnector = NYCSchoolsConnector()
    private var disposables = Set<AnyCancellable>()
    
    // Subscribe the High Schools Data
    func getNYCHighSchools() {
        // Call Data Fetch Publisher
        nycSchoolsConnector.nycSchools()
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { [weak self] value in
                    guard let self = self else { print("sink guard nil"); return }
                    switch value {
                    case .failure:
                        self.nycSchoolsResponse = nil
                        print("sink failure")
                    case .finished:
                        //print("sink finished")
                        break
                    }
                },
                receiveValue: { [weak self] nycSchoolsResponses in
                    self!.nycSchoolsResponse = nycSchoolsResponses
                    //print("receiveValue")
                })
            .store(in: &disposables)
    }
}
