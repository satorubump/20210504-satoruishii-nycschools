//
//  SchoolDetailView.swift
//  20210504-SatoruIshii-NYCSchools
//
//  Created by Satoru Ishii on 5/4/21.
//

import SwiftUI

struct SATResultsView: View {
    @ObservedObject var viewModel = SATResultsViewModel()
    var school : NYCSchoolsResponse?
    
    init(school: NYCSchoolsResponse) {
        self.school = school
        //self.viewModel.getSATResults(dbn: school.dbn)
    }
    var body: some View {
        VStack {
            Text(school!.school_name)
                .padding()
            if self.viewModel.satResultsResponse != nil && self.viewModel.satResultsResponse!.count > 0 {
                satScoresSection
            }
            mapSection
            schoolPropertySection
            Spacer()
        }
        .navigationBarTitle(ConstantsTable.SAT_RESULTS_LABEL, displayMode: .inline)
        .onAppear {
            self.viewModel.getSATResults(dbn: self.school!.dbn)
        }
    }
}

private extension SATResultsView {
    
    var mapSection : some View {
        MapView(coord: viewModel.getCoordinate(location: school!.location)!)
          .cornerRadius(25)
            .frame(height: ConstantsTable.MapHeight)
          .disabled(true)
    }

    var schoolPropertySection : some View {
        Section {
            List {
                HStack(alignment: .top) {
                    Text(ConstantsTable.LocationLabel)
                    Text(viewModel.getAddress(location: school!.location))
                }
                HStack {
                    Text(ConstantsTable.PhoneLabel)
                    Text(school!.phone_number)
                }
                HStack(alignment: .top) {
                    Text(ConstantsTable.WebsiteLabel)
                    Text(school!.website)
                }
                HStack {
                    Text(ConstantsTable.StudentsLabel)
                    Text(school!.total_students)
                        .foregroundColor(Color.black)
                }
            }
            .foregroundColor(Color.gray)
            .font(.system(size: ConstantsTable.BodyFont))
        }
    }

    var satScoresSection : some View {
        Section {
            List {
                HStack {
                    Text(ConstantsTable.TestTakersLabel)
                    Text(viewModel.satResultsResponse![0].num_of_sat_test_takers)
                        .foregroundColor(Color.black)
                }
                HStack {
                    Text(ConstantsTable.ReadingScoreLabel)
                    Text(viewModel.satResultsResponse![0].sat_critical_reading_avg_score)
                        .foregroundColor(Color.black)
                }
                HStack {
                    Text(ConstantsTable.MathScoreLabel)
                    Text(viewModel.satResultsResponse![0].sat_math_avg_score)
                        .foregroundColor(Color.black)
                }
                HStack {
                    Text(ConstantsTable.WritingScoreLabel)
                    Text(viewModel.satResultsResponse![0].sat_writing_avg_score)
                        .foregroundColor(Color.black)
                }
            }
            .foregroundColor(Color.gray)
            .font(.system(size: ConstantsTable.BodyFont))
        }
    }
}
