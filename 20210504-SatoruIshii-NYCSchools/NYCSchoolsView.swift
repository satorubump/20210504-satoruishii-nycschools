//
//  SchoolsListView.swift
//  20210504-SatoruIshii-NYCSchools
//
//  Created by Satoru Ishii on 5/4/21.
//

import SwiftUI

// The View for 2017 DOE High School Directory
struct NYCSchoolsView: View {
    // View Model
    @ObservedObject var viewModel : NYCSchoolsViewModel
    
    init(viewModel: NYCSchoolsViewModel) {
        self.viewModel = viewModel
        // Fetch the High schools data via Combine
        viewModel.getNYCHighSchools()
    }
    var body: some View {
        NavigationView {
            VStack {
                List {
                    // Schools List
                    schoolsListSection
                }
                Spacer()
            }
            .navigationBarTitle(ConstantsTable.SCHOOLS_LIST_LABEL, displayMode: .inline)
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

private extension NYCSchoolsView {
    var schoolsListSection : some View {
        Section {
            if self.viewModel.nycSchoolsResponse != nil {
                ForEach(self.viewModel.nycSchoolsResponse!, id: \.dbn) { school in
                    NavigationLink(destination: SATResultsView(school: school)) {
                        HStack {
                            Text(school.school_name)
                                .foregroundColor(Color.gray)
                                .font(.system(size: ConstantsTable.BodyFont))
                        }
                    }
                }
            }
        }
    }
}

struct SchoolsListView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = NYCSchoolsViewModel()
        NYCSchoolsView(viewModel: viewModel)
    }
}
