//
//  NYCSchoolsResponse.swift
//  20210504-SatoruIshii-NYCSchools
//
//  Created by Satoru Ishii on 5/4/21.
//

import Foundation


struct NYCSchoolsResponse : Codable {

        let dbn : String
        let school_name : String
        let location : String
        let phone_number : String
        let website : String
        let total_students : String
        
        enum CodingKeys : String, CodingKey {
            case dbn
            case school_name
            case location
            case phone_number
            case website
            case total_students
        }
}
