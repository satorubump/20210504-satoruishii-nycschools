//
//  MapView.swift
//  20210504-SatoruIshii-NYCSchools
//
//  Created by Satoru Ishii on 5/5/21.
//

import SwiftUI
import MapKit

struct MapView: UIViewRepresentable {
  private let coordinate: CLLocationCoordinate2D
  
    init(coord: CLLocationCoordinate2D) {
        self.coordinate = coord
    }
  
  func makeUIView(context: Context) -> MKMapView {
    MKMapView()
  }
  
  func updateUIView(_ view: MKMapView, context: Context) {
    let span = MKCoordinateSpan(latitudeDelta: ConstantsTable.Span, longitudeDelta: ConstantsTable.Span)
    let region = MKCoordinateRegion(center: coordinate, span: span)
    
    let annotation = MKPointAnnotation()
    annotation.coordinate = coordinate
    view.addAnnotation(annotation)
    view.setRegion(region, animated: true)
    view.isScrollEnabled = true
    view.isZoomEnabled = true
  }
}
