//
//  _0210504_SatoruIshii_NYCSchoolsApp.swift
//  20210504-SatoruIshii-NYCSchools
//
//  Created by Satoru Ishii on 5/4/21.
//

import SwiftUI

@main
struct _0210504_SatoruIshii_NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            let viewModel = NYCSchoolsViewModel()
            NYCSchoolsView(viewModel: viewModel)
        }
    }
}
