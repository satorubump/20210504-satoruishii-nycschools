//
//  ConstantsTable.swift
//  20210504-SatoruIshii-NYCSchools
//
//  Created by Satoru Ishii on 5/4/21.
//

import UIKit

struct ConstantsTable {
    static let Scheme = "https"
    static let Host = "data.cityofnewyork.us"
    static let Path = "/resource"
    static let NYCSchoolsReq = "/s3k6-pzi2.json"
    static let SATResultsReq = "/f9bf-2cp4.json"
    static let DBN = "dbn"
    
    static let SCHOOLS_LIST_LABEL = "NYC High School Directory"
    static let SAT_RESULTS_LABEL = "SAT Results"

    static let TestTakersLabel = "Num of SAT Test Takers"
    static let ReadingScoreLabel = "SAT Critical Reading AVG.Score"
    static let MathScoreLabel = "SAT Math AVG.Score"
    static let WritingScoreLabel = "SAT Writing AVG.Score"
    
    static let LocationLabel = "Location: "
    static let PhoneLabel = "Phone: "
    static let WebsiteLabel = "Website: "
    static let StudentsLabel = "Students Number: "
    
    static let Span = 0.1
    static let BodyFont : CGFloat = 18.0
    static let MapHeight : CGFloat = 160.0
}
