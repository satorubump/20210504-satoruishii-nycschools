//
//  _0210504_SatoruIshii_NYCSchoolsTests.swift
//  20210504-SatoruIshii-NYCSchoolsTests
//
//  Created by Satoru Ishii on 5/4/21.
//

import XCTest
@testable import _0210504_SatoruIshii_NYCSchools

class _0210504_SatoruIshii_NYCSchoolsTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testNYSchoolsLoading() throws {
        let viewModel = NYCSchoolsViewModel()
        viewModel.getNYCHighSchools()
        XCTAssertTrue(viewModel.nycSchoolsResponse.waitForExistence(timeout: 10))

    }
}
